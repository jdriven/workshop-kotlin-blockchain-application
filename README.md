# Kotlin: Convert Spring boot application

## 0) Before you start!
Make sure you change 2 things in the application.yml:
eureka:
  instance:
    name: <enter-your-node-name-here>
  client:
    serviceUrl:
      defaultZone: http://<enter-the-eureka-server-ip-here>:8761/eureka/
      
## 1) Getting started: build.gradle

In order to get started we need to add some dependencies to our build script. We need to add at least 2 things:
* the kotlin gradle plugin
* the kotlin libraries

Replace the following closures in the buildscript {}
```
ext {
  kotlinVersion = '1.3.60'
  springBootVersion = '2.2.1.RELEASE'
}
dependencies {
  classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
  classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${kotlinVersion}")
}
```

Replace the `java` plugin with the `kotlin` the plugin:
```
apply plugin: 'kotlin'
```

Add the kotlin libraries to the dependencies {}
```
compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
compile("org.jetbrains.kotlin:kotlin-reflect")
```

You spring application should now still be able to startup. 
Verify this by checking out and running the `NodeApplicationTest`
```
git checkout NodeApplicationTest
```


## 2) Convert a file to kotlin
If you're using IntelliJ you can use IntelliJ to convert java files to kotlin.
While you have the `NodeApplicationTest` opened, use the search function (Ctrl + Shift + A) to look for 'Convert Java File to Kotlin File'
or use Ctrl+Alt+Shift+K to convert the file to kotlin.
It should now look like:

```kotlin
@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = arrayOf(NodeApplication::class))
@ActiveProfiles("test")
class NodeApplicationTest {

    @Autowired
    private val applicationContext: ApplicationContext? = null

    @Test
    fun applicationStartsUp() {
        val bean = applicationContext!!.getBean(BlockChainService::class.java)
        assertNotNull(bean)
        // I know, it's a silly test. Just for illustration...
        assertEquals("nl.jdriven.blockchainminer.service.BlockChainService",
                applicationContext.getType("blockChainService").name)
    }
}
```

Feel free to run it and observe it still works.
The `!!` after applicationContext will still throw a NullPointerException if the applicationContext would be `null`. 
However, we know that this shouldn't happen. The `!!` is shorthand for: "I know i'm accessing a nullable property, but I know it's there. Throw a NullPointerException otherwise"
A way around this is to declare the applicationContext as a `lateinit var`. Try it yourself.

Java libraries can provide [a set of annotations (such as @Nullable)](https://kotlinlang.org/docs/reference/java-interop.html#nullability-annotations) so you can leverage the null-safety system of Kotlin.
These annotations are added to Spring in order to support kotlin. Note that the the applicationContext.getType method has this interface:
```java
	@Nullable
	Class<?> getType(String name) throws NoSuchBeanDefinitionException;
```
But our test did still compile eventhough we directly accessed a property (name) on a nullable type, right?
That's because you can decide it for yourself how this interoperability should work.

Add these blocks to your build.gradle (should at least be below the kotlin plugin):
```groovy
compileKotlin {
  kotlinOptions {
    freeCompilerArgs = ["-Xjsr305=strict"]
    jvmTarget = "1.8"
  }
}
compileTestKotlin {
  kotlinOptions {
    freeCompilerArgs = ["-Xjsr305=strict"]
    jvmTarget = "1.8"
  }
}
```
Now the `NodeApplicationTest` file doesn't compile anymore (you might want to refresh your gradle projects)
This should give you a compilation error: "Error:(29, 64) Kotlin: Only safe (?.) or non-null asserted (!!.) calls are allowed on a nullable receiver of type Class<*>?"
This means you have 2 options:
- use the non-null asserted accessor !!.name 
- or the safe accessor ?.name. 
Either of those will fail your UT if the result would be null, so it's up to you. Usually !! is avoided.
Unfortunately (?) if there are no @Nullable/@Nonnull annotations provided by the java library, there is no guaranteed null-safety. To this date, there is no flag that you can use to force you to check all objects coming from a java library. They are all considered as not-null if no annotations are provided.

##  [3) Spring Framework 5 Kotlin APIs, the functional way](https://spring.io/blog/2017/08/01/spring-framework-5-kotlin-apis-the-functional-way)
Kotlin can be used very well to create a DSL. 
There are a couple of features that contribute to the fact that you can create nice DSLs in Kotlin:
- Use of lambdas outside of method parentheses
- Lambdas with receivers
- Extension functions
- infix functions
- and probably much more.

For example, you can create type safe builders with these capabilities: https://kotlinlang.org/docs/reference/type-safe-builders.html
There is another example in spring where Kotlin DSL can be used to register beans / create applicationcontext in a concise way.
Spring 5 is aiming on a more functional way of doing things, as opposed to have magic happening behind the scenes with annotations.
 
 #### The application context
 It is possible to entirely get rid off all annotations, but for now we still use `@SpringBootApplication` because we're migrating an existing application. As you will see you can easily exchange (and have both at the same time) the 'old' java classes with annotations to functional kotlin declarations.
 For an example without any annotations [see here](https://spring.io/blog/2017/08/01/spring-framework-5-kotlin-apis-the-functional-way)
 
 Let's use a functional approach to register some beans and define some routes.
 ```
git checkout beans
```
 1) replace the `spring-boot-starter-web` dependency for `compile('org.springframework.boot:spring-boot-starter-webflux')`  
 2) remove the webConfiguration class (this doesn't compile after step 1)
 3) convert the NodeApplication to kotlin. IntelliJ will create a mess, so you can copy paste something like this:
 

```kotlin
@SpringBootApplication
open class NodeApplication

fun main(args: Array<String>) {

    val application = SpringApplication(NodeApplication::class.java)
    application.addInitializers(beans())
    application.run(*args)
}
```

Gotcha: The class is marked 'open' because @Configuration classes (@SpringBootApplication) might be subclassed by spring. That's not possible for a normal class since in Kotlin everything is final by default. You have 2 options:
1) mark the class open
2) use a gradle plugin to 'open up' all classes. To do so: add `classpath("org.jetbrains.kotlin:kotlin-allopen:${kotlinVersion}")` to the buildscript.dependencies and `apply plugin: 'kotlin-spring'`

Start up your application with
`gradlew bootRun` and see that netty (! not tomcat) is started.

#### register beans
```
git checkout beanstest
```
You can find a lot of demo's about registering beans with kotlin the functional way. Demos always look nice, but it seems that this is still in it's infancy. The following will explain what is meant by that.

Adding the 'beans' in a functional way, as ApplicationContextInitializer, is clear and easy. ( this is what you did with `application.addInitializers(beans())`)
HOWEVER, these beans will not be registered if you create a @SpringBootTest. 
(On date of writing, https://github.com/spring-projects/spring-boot/issues/8115)

If the ApplicationContextInitializer is listed in `META-INF/spring.factories` it does get loaded. However, this is not easy since you will miss other spring-beans and properties that you are dependent on. 

The final workaround for this migration is to create a custom ApplicationContextInitializer and refer to it in the application.yml:

```yaml
context:
  initializer:
    classes: nl.jdriven.blockchainminer.BeansInitializer
```

Your main class should now look like: 

```kotlin
fun main(args: Array<String>) {
    val application = SpringApplication(NodeApplication::class.java)
    application.run(*args)
}

class BeansInitializer : ApplicationContextInitializer<GenericApplicationContext> {
    override fun initialize(context: GenericApplicationContext) =
            beans().initialize(context)
}
```

Register all the beans in beans.kt and make sure BeansTest completes succesfully.
You can get rid of all the annotations.
hint: if you need to call a constructor you can use `ref()` to get a reference to the required bean that should get injected in the constructor.
hint2: you might not even need it. A simple bean<TypeOfBean>() is enough
hint3: the NodeApplicationTest might fail since the name of the bean will now contain the package as well.
Therefore `applicationContext.getType("BlockChainService")` does not result a bean. You can prefix it with the package.

#### registering routes
```
git checkout routes
```
Besides registering beans programatically, we can also add routes the functional way.
An advantage is less magic under the hood, ability to debug, and it's more flexible compared to annotations.

Register all the routes. An example on how to is listed [here](https://spring.io/blog/2017/08/01/spring-framework-5-kotlin-apis-the-functional-way#functional-routing-with-the-kotlin-dsl-for-spring-webflux)
Convert the controllers to kotlin and make sure all methods return a `Mono<ServerResponse>`
You can run the ControllerTest to verify your code.

tip1: you can create extension functions wherever you want. For exmple you could define the following function to:
```kotlin
   /**
     * Extension function.
     * So we can have a more 'easy' interface like ServerResponse().ok().withBody(anyObject)
     */
    private fun ServerResponse.BodyBuilder.withBody(o: Any): Mono<ServerResponse> {
        return this.body(org.springframework.web.reactive.function.BodyInserters.fromObject(o))
    }
```

This allows you to return a response in the following way:
```kotlin
return ServerResponse.ok().withBody(..your object..)
```

tip2: Never do a blocking operation. You can use 'doOnNext' for example. A hint:
tip3: One example: In Routes.kt:
```kotlin
                    POST("/message", nodeController::message)
```
In NodeController.kt
```kotlin
    fun message(req: ServerRequest): Mono<ServerResponse> {

        return req.bodyToMono(Message::class.java)
                .doOnNext { body -> log.info("body: $body") }
                .doOnNext { message -> blockChain.addMessage(message) }
                .doOnNext { log.info("adding message done") }
                .flatMap { ServerResponse.ok().build() }
    }
```
## Convert domain objects
```
git checkout domainobjects
```
One of the great features of kotlin is a data-class.
we can get rid of a lot of boilerplate by converting the Block.java and Message.java to kotlin.
hint1: your IntelliJ 'convert to kotlin' function is not smart enough to make a dataclass, you will have to make your own `data class Message` and `data class Block`
If you already tried to run Unit tests, you see that Jackson isn't able to cope with these classes.
In order to solve that, add the following dependency:
`compile('com.fasterxml.jackson.module:jackson-module-kotlin')`

Block.java: You should only need a constructor (primary, at class declaration), and a 'companion object' to declare the ZERO constant.
Message.java: You should only need a constructor(primary, at class declaration), and implementing the Comparable interface. (this function declaration can easily be a single expression function oneliner)

BlockChain.java: You get this for free, but feel free to implement it yourself or improve it.
```kotlin
class BlockChain {

    private val log = LoggerFactory.getLogger(BlockChain::class.java)
    private val blocks: MutableMap<String, TreeNode<Block>> = HashMap()
    var currentEndBlock: TreeNode<Block>
        private set
    private var currentDepth = 0


    init {
        val root = TreeNode(null, Block.ZERO, 0)
        blocks["0"] = root
        currentEndBlock = root
    }
    

    fun containsBlock(b: Block): Boolean = blocks.containsKey(b.hash)

    fun blocksInLongestChain(): List<Block> {
        val blocksInChain = ArrayList<Block>()

        var b = currentEndBlock

        while (b.parent != null) {
            blocksInChain.add(b.data)
            b = b.parent
        }
        blocksInChain.add(b.data) // Add the root.
        return blocksInChain
    }

    fun getOrphanedBlocks(): List<Block> {
        val blocksInLongestChain = blocksInLongestChain()
        return getAllBlocks().filter { !blocksInLongestChain.contains(it) }
    }

    fun getAllBlocks(): List<Block> = blocks.values.map { it.data }


    fun addBlock(block: Block, onNewLongestChain: (newDepth: Int) -> Unit = {}) {
        val parentBlock = blocks[block.parentHash]
        if (parentBlock == null) {
            blocks[block.hash] = TreeNode(null, Block.ZERO, 0)// Adding a new block without parent
            return
        }

        val newNode = parentBlock.addChild(block)

        if (newNode.depth > currentDepth) {

            currentDepth = newNode.depth
            currentEndBlock = newNode
            onNewLongestChain(newNode.depth)
        } else {

            log.info("Unfortunately, a block was added, but does not belong to the longest chain.")

        }
        blocks[block.hash] = newNode
    }
}
```

After converting the BlockChain to Kotlin you will notice that the BlockChainService doesn't compile anymore. For more information [click here](https://discuss.kotlinlang.org/t/java-interop-unit-closures-required-to-return-kotlin-unit/1842/7) The lambda's in java should return void, but in Kotlin we have 'Unit'. You can fix this for now (workaround) by returning a 'Unit' instance:
```java
   chain.addBlock(block, (depth) -> {
                log.info("added a block! new depth:" + depth);
                return Unit.INSTANCE;
            });
```
 

## Sealed class
We can cleanup the code a little by using a ['sealed class'](https://kotlinlang.org/docs/reference/sealed-classes.html).
Lets use this for the 'TreeNode' class.
Create a kotlin file 'TreeNode.kt' and create 3 classes:
1) TreeNode, should have val data:T, val depth: Int in constructor and an array of treenodes (the children)
2) RootNode, should extend TreeNode  
3) LeafNode, should extend TreeNode and have an additional 'parent' constructor property.

We can now modify BlockChain by replacing
```kotlin
val root = TreeNode(null, Block.ZERO, 0)
``` 
```kotlin
val root = RootNode(Block.ZERO)
```
and 
```kotlin
 while (b.parent != null) {
```
to
```kotlin
 while (b is LeafNode) {
```

Also, convert the blockChainService by IntelliJ (Ctrl+Shift+A 'convert java file to kotlin file')
Fix the compilation errors. (IntelliJ is apparently not thát smart)
hint1: A class and function with `Async` is subclassed by spring, so you should mark it as 'open'.
hint2: for some reason the getData() is not replaced to 'data'
hint3: you don't need to declare a 'Consumer'. Place the lambda in subscribe:
```kotlin
    .subscribe({ this.addCreatedBlock(it) })
or  .subscribe(this::addCreatedBlock)
```
hint4: The collection library can make your life easy. For example with:
```kotlin
    val messageForNextBlock = unhandledMessages.take(5).toSet()
```

Your unit tests should still work.
The refactored code by IntelliJ can still be improved a lot, especially:
- The large `init{}` block is unnecessary
- functions with iterations can be improved a lot (addCreatedBlock, resetMessagesAccordingToChain)
Give it a try!

## convert it all.
You can now also convert the BlockCreatorService, ParameterService, JChainHasher.
Good luck! :)
