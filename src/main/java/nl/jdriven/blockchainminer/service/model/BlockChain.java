package nl.jdriven.blockchainminer.service.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class BlockChain {

    private final TreeNode<Block> root;
    private final Map<String, TreeNode<Block>> blocks;
    private TreeNode<Block> currentEndBlock;
    private int maxDepth = 0;

    private Logger log = LoggerFactory.getLogger(BlockChain.class);

    public BlockChain() {
        root = new TreeNode<>(null, Block.ZERO, 0);
        blocks = new HashMap<>();
        blocks.put("0", root);
        currentEndBlock = root;
        maxDepth = 0;
    }

    public void addBlock(final Block block) {
        addBlock(block, integer -> log.info("succesfully added new block. chainlength: " + integer));
    }

    public void addBlock(final Block block, Consumer<Integer> onSuccessFullyAddBlockConsumer) {
            final TreeNode<Block> parentBlock = blocks.get(block.getParentHash());
            if (parentBlock == null) {
                final TreeNode<Block> newNode = new TreeNode<>(null, block, 0);
                blocks.put(block.getHash(), newNode);
            } else {
                final TreeNode<Block> newNode = parentBlock.addChild(block);
                if (newNode.getDepth() > maxDepth) {
                    maxDepth = newNode.getDepth();
                    onSuccessFullyAddBlockConsumer.accept(newNode.getDepth());
                    currentEndBlock = newNode;
                } else {
                    log.info("Unfortunately, a block was added, but does not belong to the longest chain.");
                }
                blocks.put(block.getHash(), newNode);
            }
    }

    public boolean containsBlock(final Block b) {
        return blocks.containsKey(b.getHash());
    }

    public TreeNode<Block> getCurrentEndBlock() {
        return currentEndBlock;
    }

    public List<Block> getOrphanedBlocks() {
        List<Block> blocksInChain = blocksInLongestChain();
        return this.blocks.values().stream()
                .map(TreeNode::getData)
                .filter(block -> !blocksInChain.contains(block))
                .collect(Collectors.toList());
    }

    public List<Block> getAllBlocks() {
        List<Block> allBlocks = new ArrayList<>();
        for (TreeNode<Block> blockTreeNode : blocks.values()) {
            allBlocks.add(blockTreeNode.getData());
        }
        return allBlocks;
    }

    public List<Block> blocksInLongestChain() {
        final List<Block> blocksInChain = new ArrayList<>();
        TreeNode<Block> b = currentEndBlock;
        do {
            blocksInChain.add(b.getData());
            b = b.getParent();
        } while (b != null);
        return blocksInChain;
    }
}
