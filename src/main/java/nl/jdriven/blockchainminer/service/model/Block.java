package nl.jdriven.blockchainminer.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public final class Block {
    public static final Block ZERO;

    static {
        ZERO = new Block("0", null, new LinkedHashSet<>(), "0", null);
    }

    private final String hash;
    private final String parentHash;
    private final Set<Message> contents;
    private final String nonce;
    private final String discoveredBy;

    public Block(@JsonProperty("hash") final String hash,
                 @JsonProperty("parentHash") final String parentHash,
                 @JsonProperty("content") final Set<Message> content,
                 @JsonProperty("nonce") final String nonce,
                 @JsonProperty("discoveredBy") final String discoveredBy) {
        this.hash = hash;
        this.parentHash = parentHash;
        this.contents = new LinkedHashSet<>(content);
        this.nonce = nonce;
        this.discoveredBy = discoveredBy;
    }

    public String getDiscoveredBy() {
        return discoveredBy;
    }

    public String getHash() {
        return hash;
    }

    public String getParentHash() {
        return parentHash;
    }

    public Set<Message> getContent() {
        return new LinkedHashSet<>(contents);
    }

    public String getNonce() {
        return nonce;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("hash", hash)
                .add("parentHash", parentHash)
                .add("contents", contents)
                .add("nonce", nonce)
                .add("discoveredBy", discoveredBy)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Block block = (Block) o;
        return Objects.equals(hash, block.hash) &&
                Objects.equals(parentHash, block.parentHash) &&
                Objects.equals(contents, block.contents) &&
                Objects.equals(nonce, block.nonce) &&
                Objects.equals(discoveredBy, block.discoveredBy);
    }

    @Override
    public int hashCode() {

        return Objects.hash(hash, parentHash, contents, nonce, discoveredBy);
    }
}
