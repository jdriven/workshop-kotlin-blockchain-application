package nl.jdriven.blockchainminer.service;

import io.reactivex.Maybe;
import nl.jdriven.blockchainminer.hasher.JChainHasher;
import nl.jdriven.blockchainminer.service.model.Block;
import nl.jdriven.blockchainminer.service.model.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.Set;


@Service
public class BlockCreatorService {

    private final ParameterService parameterService;
    private final JChainHasher jChainHasher;

    @Value("${eureka.instance.name}")
    private String name;

    public enum State {
        READY,
        RUNNING,
        CANCELLED
    }

    private final Random random;
    private State state;


    public BlockCreatorService(final ParameterService parameterService, final JChainHasher jChainHasher) {
        this.parameterService = parameterService;
        this.jChainHasher = jChainHasher;
        this.random = new Random();
        this.state = State.READY;
    }

    Maybe<Block> createBlock(Block parentBlock, Set<Message> messages) {
        state = State.RUNNING;

        return Maybe.create(maybeEmitter -> {
            String hash;
            String parentHash = parentBlock.getHash();
            long nonce = random.nextLong();
            do {
                hash = jChainHasher.hash(parentHash, messages, Long.toString(++nonce));
                try {
                    Thread.sleep(parameterService.getDifficulty());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            } while (!jChainHasher.isValidHash(hash) && state.equals(State.RUNNING));

            if (state.equals(State.RUNNING)) {

                maybeEmitter.onSuccess(new Block(hash, parentHash, messages, Long.toString(nonce), name));
            } else {
                maybeEmitter.onComplete();
            }

            state = State.READY;
        });
    }

    public State getState() {
        return state;
    }
    public void setState(State state) {
        this.state = state;
    }

    public void cancelRun() {
        this.state = State.CANCELLED;
    }

}
