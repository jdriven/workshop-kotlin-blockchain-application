package nl.jdriven.blockchainminer.service;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import nl.jdriven.blockchainminer.hasher.JChainHasher;
import nl.jdriven.blockchainminer.service.model.Block;
import nl.jdriven.blockchainminer.service.model.BlockChain;
import nl.jdriven.blockchainminer.service.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BlockChainService {

    private Logger log = LoggerFactory.getLogger(BlockChainService.class);

    @Value("${eureka.instance.instanceId}")
    private String instanceId;

    private final Set<Message> unhandledMessages;
    private final Set<Message> handledMessages;
    private final BlockChain chain;
    private final BlockCreatorService blockCreatorService;
    private final JChainHasher jChainHasher;
    private final EurekaClient eurekaClient;

    @Autowired
    public BlockChainService(final BlockCreatorService blockCreatorService, final JChainHasher jChainHasher, final EurekaClient eurekaClient) {
        this.blockCreatorService = blockCreatorService;
        this.jChainHasher = jChainHasher;
        this.eurekaClient = eurekaClient;
        this.unhandledMessages = new HashSet<>();
        this.handledMessages = new HashSet<>();
        this.chain = new BlockChain();
        start();
    }

    private void start() {
        new Thread(() -> {
            while(true) {
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                createNewBlockIfReady();
            }
        }).start();

    }

    public BlockChain getChain() {
        return chain;
    }

    public Set<Message> getUnprocessedMessages() {
        return unhandledMessages;
    }

    public void addMessage(final Message m) {
        if (!handledMessages.contains(m) && !unhandledMessages.contains(m)) {
            unhandledMessages.add(m);
        }
    }

    public void addBlock(final Block block) {
        final String hash = jChainHasher.hash(block.getParentHash(), block.getContent(), block.getNonce());
        if (!jChainHasher.isValidHash(hash)) {
            throw new IllegalArgumentException("Rejected block!: hash is invalid for " + block + " calculated hash: " + hash);
        }
        if (!block.getHash().equals(hash)) {
            throw new IllegalArgumentException("Rejected block!: hash is different. blockhash: " + block.getHash() + " calculated hash:" + hash);
        }
        if (!chain.containsBlock(block)) {
            final String lastBlockHash = chain.getCurrentEndBlock().getData().getHash();
            chain.addBlock(block, (depth) -> {
                log.info("added a block! new depth:" + depth);
            });
            if (!chain.getCurrentEndBlock().getData().getHash().equals(lastBlockHash)) {
                if (blockCreatorService.getState() == BlockCreatorService.State.RUNNING) {
                    blockCreatorService.cancelRun();
                }
                resetMessagesAccordingToChain();
                blockCreatorService.setState(BlockCreatorService.State.READY);
            }
        }

    }

    private synchronized  void createNewBlockIfReady() {
        if (isReadyForNewBlock()) {
            createNewBlock();
        }
    }

    private void addCreatedBlock(final Block block) {
        chain.addBlock(block, (depth) -> {
            log.info("added a block! new depth:" + depth);
        });
        final Application application = eurekaClient.getApplication("jchain-node");
        final List<InstanceInfo> instanceInfo = application.getInstances();
        for (InstanceInfo info : instanceInfo) {
            if (info.getInstanceId().equals(instanceId)) {
                continue;
            }
            informNodeOfNewBlock(info.getHostName() + ":" + Integer.toString(info.getPort()), block);
        }
    }

    private void resetMessagesAccordingToChain() {
        unhandledMessages.addAll(handledMessages);
        handledMessages.clear();
        List<Block> blocks = chain.blocksInLongestChain();
        for (Block block : blocks) {
            for (Message m : block.getContent()) {
                unhandledMessages.remove(m);
                handledMessages.add(m);
            }
        }
    }

    private Set<Message> pickMessagesForPotentialBlock() {
        final Set<Message> messageForNextBlock = unhandledMessages.stream()
                .limit(5)
                .collect(Collectors.toSet());
        unhandledMessages.removeAll(messageForNextBlock);
        handledMessages.addAll(messageForNextBlock);
        return messageForNextBlock;
    }

    private boolean isReadyForNewBlock() {
        return unhandledMessages.size() >= 5 && blockCreatorService.getState() == BlockCreatorService.State.READY;
    }

    private void createNewBlock() {
        final Set<Message> blockContent = pickMessagesForPotentialBlock();
        blockCreatorService.createBlock(chain.getCurrentEndBlock().getData(), blockContent).subscribe(this::addCreatedBlock);
    }

    @Async
    void informNodeOfNewBlock(final String host, final Block block) {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject("http://" + host + "/node/block", block, Block.class);
    }
}
