package nl.jdriven.blockchainminer.hasher;

import nl.jdriven.blockchainminer.service.ParameterService;
import nl.jdriven.blockchainminer.service.model.Message;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Service
public class JChainHasher {

    private ParameterService parameterService;

    public JChainHasher(final ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    public String hash(final String parentHash, final Set<Message> content, final String nonce) {
        final MessageDigest messageDigest = getMessageDigest();
        TreeSet<Message> messages = new TreeSet<>(content);
        final String blockData = new StringBuilder()
                .append(parentHash)
                .append(messages.toString())
                .append(nonce)
                .toString();
        messageDigest.update(blockData.getBytes());
        return new String(Base64.getEncoder().encode(messageDigest.digest()), StandardCharsets.UTF_8);
    }

    private MessageDigest getMessageDigest() {
        try {
            return MessageDigest.getInstance(parameterService.getEncryptionAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            /*
             * This exception may never be thrown.
             *
             * Every implementation of the Java platform is required to support the following standard MessageDigest algorithms:
             * MD5
             * SHA-1
             * SHA-256
             */
            throw new RuntimeException("Java platform does not support standard encryption", e);
        }
    }

    public boolean isValidHash(final String hash) {
        if (null == hash) {
            return false;
        }
        int difficulty = parameterService.getDifficulty();

        char[] chars = hash.toCharArray();
        for (int i = 0; i < difficulty; i++) {
            if (chars[i] != '0') {
                return false;
            }
        }

        return true;
    }
}
