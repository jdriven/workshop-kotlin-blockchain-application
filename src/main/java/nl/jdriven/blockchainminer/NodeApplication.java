package nl.jdriven.blockchainminer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(nl.jdriven.blockchainminer.NodeApplication.class, args);
    }
}
