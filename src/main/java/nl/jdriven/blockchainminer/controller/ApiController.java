package nl.jdriven.blockchainminer.controller;

import nl.jdriven.blockchainminer.service.BlockChainService;
import nl.jdriven.blockchainminer.service.ParameterService;
import nl.jdriven.blockchainminer.service.model.Block;
import nl.jdriven.blockchainminer.service.model.BlockChain;
import nl.jdriven.blockchainminer.service.model.Message;
import nl.jdriven.blockchainminer.service.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/api")
public class ApiController {

    private final BlockChainService blockChainService;
    private final ParameterService parameterService;

    @Autowired
    public ApiController(final BlockChainService blockChainService, final ParameterService parameterService) {
        this.blockChainService = blockChainService;
        this.parameterService = parameterService;
    }

    @GetMapping(path = "/fullchain")
    public List<Block> fullChain() {
        return blockChainService.getChain().getAllBlocks();
    }

    @GetMapping(path = "/chain")
    public List<Block> chain() {
        final BlockChain blockChain = blockChainService.getChain();
        final List<Block> mainChain = new LinkedList<>();
        TreeNode<Block> b = blockChain.getCurrentEndBlock();
        do {
            mainChain.add(b.getData());
            b = b.getParent();
        } while (b != null);
        Collections.reverse(mainChain);
        return mainChain;
    }

    @GetMapping(path = "/orphaned")
    public List<Block> orphaned() {
        return blockChainService.getChain().getOrphanedBlocks();
    }

    @GetMapping(path = "/messages")
    public Set<Message> messages() {
        return blockChainService.getUnprocessedMessages();
    }

    @GetMapping(path = "/algorithm")
    public String getAlgorithm() {
        return "\"" + parameterService.getEncryptionAlgorithm() + "\"";
    }

    @GetMapping(path = "/difficulty")
    public int getDifficulty() {
        return parameterService.getDifficulty();
    }

    @GetMapping(path = "/difficulty/{number}")
    public void setDifficulty(@PathVariable("number") int difficulty) {
        parameterService.setDifficulty(difficulty);
    }

}
